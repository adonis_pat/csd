require "./calculadora"

describe Calculadora do

	before(:each) do
		@mi_calc = Calculadora.new
	end

	it "debe dar 4 cuando sumo 2 mas 2" do
		resultado = @mi_calc.sumar(2,2)
		resultado.should == 4
	end

	it "debe dar 3 cuando sumo 8 mas -5" do
		resultado = @mi_calc.sumar(8,-5)
		resultado.should == 3
	end

end

