require 'sinatra'
require './lib/saludador'

get '/' do
  erb :bienvenido
end

post '/saludar' do
  nombre = params[:nombre]
  saludador = Saludador.new
  @nombre = saludador.procesar(nombre)
  erb :saludo
end
